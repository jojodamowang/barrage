//package top.houry.netty.barrage.netty;
//
//import cn.hutool.core.util.ObjectUtil;
//import cn.hutool.core.util.StrUtil;
//import io.netty.channel.ChannelHandlerContext;
//import io.netty.channel.ChannelInboundHandlerAdapter;
//import io.netty.channel.SimpleChannelInboundHandler;
//import io.netty.handler.timeout.IdleStateEvent;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang3.StringUtils;
//import top.houry.netty.barrage.consts.BarrageChannelAttrConst;
//import top.houry.netty.barrage.consts.BarrageRedisKeyConst;
//import top.houry.netty.barrage.proto.BarrageProto;
//import top.houry.netty.barrage.service.impl.BarrageWatchInfoServiceImpl;
//import top.houry.netty.barrage.utils.BarrageMsgBeanUtils;
//import top.houry.netty.barrage.utils.BarrageRedisUtils;
//import top.houry.netty.barrage.utils.BarrageSpringContextUtil;
//
//import java.net.InetSocketAddress;
//import java.util.concurrent.TimeUnit;
//
///**
// * @Desc 配置netty-handler
// * @Author houry
// * @Date 2021/3/2 9:30
// **/
//@Slf4j
//public class WebSocketNettyServerFlowControlHandler extends ChannelInboundHandlerAdapter {
//
//    /**
//     * 读取发送的消息
//     *
//     * @param ctx     通道上下文
//     * @param barrage 信息内容
//     * @throws Exception
//     */
//    @Override
//    public void channelRead(ChannelHandlerContext ctx, Object barrage) throws Exception {
//        InetSocketAddress ipSocket = (InetSocketAddress) ctx.channel().remoteAddress();
//        String clientIp = ipSocket.getAddress().getHostAddress();
//        String rejectCache = BarrageRedisUtils.get(BarrageRedisKeyConst.BARRAGE_SERVER_REJECT_CONNECT_KEY + clientIp);
//        if (StrUtil.isNotBlank(rejectCache)) {
//            log.info("WebSocketNettyServerFlowControlHandler-channelRead-ip:{}-黑名单-访问直接断开", clientIp);
//            ctx.channel().close();
//        } else {
//            String count = BarrageRedisUtils.get(BarrageRedisKeyConst.BARRAGE_SERVER_FLOW_CONTROL + clientIp);
//            String key = BarrageRedisKeyConst.BARRAGE_SERVER_FLOW_CONTROL + clientIp;
//            if (StrUtil.isBlank(count)) {
//                String countStr = BarrageRedisUtils.get(key);
//                if (StringUtils.isBlank(countStr)) {
//                    BarrageRedisUtils.set(key, "1", 10L, TimeUnit.SECONDS);
//                }
//                ctx.fireChannelRead(barrage);
//            } else {
//                int newCount = ObjectUtil.defaultIfNull(Integer.parseInt(BarrageRedisUtils.get(key)), 0) + 1;
//                BarrageRedisUtils.set(key, String.valueOf(newCount), 10L, TimeUnit.SECONDS);
//                if (newCount > 100) {
//                    BarrageRedisUtils.set(BarrageRedisKeyConst.BARRAGE_SERVER_REJECT_CONNECT_KEY + clientIp, clientIp, 365L, TimeUnit.DAYS);
//                    log.info("WebSocketNettyServerFlowControlHandler-channelRead-ip:{}-已经加入黑名单", clientIp);
//                    ctx.channel().close();
//                } else {
//                    ctx.fireChannelRead(barrage);
//                }
//            }
//        }
//
//
//    }
//
//
//}
