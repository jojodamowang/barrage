package top.houry.netty.barrage.netty;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import top.houry.netty.barrage.consts.BarrageRedisKeyConst;
import top.houry.netty.barrage.utils.BarrageRedisUtils;

import java.net.InetSocketAddress;
import java.util.concurrent.TimeUnit;


@Slf4j
public class WebSocketNettyServerTrafficStatisticsHandler extends ChannelInboundHandlerAdapter {

    /**
     * 读取发送的消息
     *
     * @param ctx     通道上下文
     * @param barrage 信息内容
     * @throws Exception
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object barrage) throws Exception {
        InetSocketAddress ipSocket = (InetSocketAddress) ctx.channel().remoteAddress();
        String clientIp = ipSocket.getAddress().getHostAddress();
        String count = BarrageRedisUtils.get(BarrageRedisKeyConst.BARRAGE_SERVER_FLOW_CONTROL_KEY + clientIp);
        if (StrUtil.isBlank(count)) {
            String countStr = BarrageRedisUtils.get(BarrageRedisKeyConst.BARRAGE_SERVER_FLOW_CONTROL_KEY + clientIp);
            if (StringUtils.isBlank(countStr)) {
                BarrageRedisUtils.set(BarrageRedisKeyConst.BARRAGE_SERVER_FLOW_CONTROL_KEY + clientIp, "1", 10L, TimeUnit.SECONDS);
            }
        } else {
            int newCount = ObjectUtil.defaultIfNull(Integer.parseInt(BarrageRedisUtils.get(BarrageRedisKeyConst.BARRAGE_SERVER_FLOW_CONTROL_KEY + clientIp)), 0) + 1;
            BarrageRedisUtils.set(BarrageRedisKeyConst.BARRAGE_SERVER_FLOW_CONTROL_KEY + clientIp, String.valueOf(newCount), 10L, TimeUnit.SECONDS);
        }
        ctx.fireChannelRead(barrage);
    }


}
