package top.houry.netty.barrage.consts;

import io.netty.util.AttributeKey;

public interface BarrageChannelAttrConst {

    AttributeKey<String> NETTY_CHANNEL_VIDEO_ID_ATTR = AttributeKey.valueOf("videoId");
}
